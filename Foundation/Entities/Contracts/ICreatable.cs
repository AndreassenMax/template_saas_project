using System;

namespace Foundation.Entities.Contracts
{
    public interface ICreatable
    {
        DateTime? CreatedAt { get; set; }
    }
}