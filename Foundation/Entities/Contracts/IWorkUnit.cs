using System.Threading.Tasks;

namespace Foundation.Entities.Contracts
{
    public interface IWorkUnit
    {
        void Commit();

        Task CommitAsync();
    }
}