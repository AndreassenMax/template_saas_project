using System;

namespace Foundation.Entities.Contracts
{
    public interface IUpdatable
    {
        DateTime? UpdatedAt { get; set; }
    }
}