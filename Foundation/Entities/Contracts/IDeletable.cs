using System;

namespace Foundation.Entities.Contracts
{
    public interface IDeletable
    {
        DateTime? DeletedAt { get; set; }
    }
}