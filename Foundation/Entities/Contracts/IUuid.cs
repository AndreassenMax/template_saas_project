using System;

namespace Foundation.Entities.Contracts
{
    public interface IUuid
    {
        Guid Uuid { get; set; }
    }
}