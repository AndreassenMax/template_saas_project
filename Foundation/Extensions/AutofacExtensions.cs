using System;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Extensions.DependencyInjection;
using Autofac.Features.Scanning;
using Foundation.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Foundation.Extensions
{
    public static class AutofacExtensions
    {
        public static ContainerBuilder AddAutofacWithFoundation(this ContainerBuilder builder)
        {
            builder.ConfigureFoundationDependencies();
            
            return builder;
        }

        private static void ConfigureFoundationDependencies(this ContainerBuilder builder)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                        
            builder.RegisterAssemblyTypes(assemblies)
                .WithAttribute<FoundationServiceAttribute>()
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }

        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> WithAttribute<TAttribute>(
            this IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> builder) where TAttribute : Attribute
        {
            return builder.Where(e => e.GetCustomAttribute<TAttribute>() != null);
        }
    }
}