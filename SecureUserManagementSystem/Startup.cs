using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SecureUserManagementSystem.Data;
using SecureUserManagementSystem.Extensions;
using Foundation.Extensions;
using SecureUserManagementSystem.Security;
using SecureUserManagementSystem.Security.Handlers;

namespace SecureUserManagementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContextConfiguration(Configuration)
                .AddControllers();

            services.AddHttpContextAccessor();

            services.AddAuthentication(o =>
                {
                    o.DefaultScheme = Schemes.TokenAuthenticationDefaultScheme;
                })
                .AddScheme<TokenOptions, TokenHandler>(Schemes.TokenAuthenticationDefaultScheme, o => { });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder
                .AddAutofacWithFoundation()
                .ConfigureDependencies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var container = app.ApplicationServices.GetAutofacRoot();
            
            container.Resolve<UserManagementDbContext>()
                .Database
                .Migrate();
        }
    }
}
