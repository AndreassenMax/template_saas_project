﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecureUserManagementSystem.Domain.Users.Services.Contracts;
using SecureUserManagementSystem.Security;

namespace SecureUserManagementSystem.Controllers
{
    [Route("api/test")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IUserRegistrationService _userRegistrationService;

        public TestController(IUserRegistrationService userRegistrationService)
        {
            _userRegistrationService = userRegistrationService;
        }
        
        [HttpGet]
        [Route("auth")]
        [Authorize(AuthenticationSchemes = Schemes.TokenAuthenticationDefaultScheme)]
        public async Task<IActionResult> AuthTestAsync()
        {
            return Ok(await _userRegistrationService.TestAsync());
        }
    }
}
