﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SecureUserManagementSystem.Domain.Users.Requests;
using SecureUserManagementSystem.Domain.Users.Services.Contracts;

namespace SecureUserManagementSystem.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private readonly IUserRegistrationService _userRegistrationService;
        private readonly ILoginService _loginService;

        public SecurityController(
            IUserRegistrationService userRegistrationService,
            ILoginService loginService
            )
        {
            _userRegistrationService = userRegistrationService;
            _loginService = loginService;
        }
        
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegistrationRequest request)
        {
            var result = await _userRegistrationService.RegisterAsync(request);

            if (result)
                return Ok();
            
            return BadRequest();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginRequest request)
        {
            var user = await _loginService.GetUserAsync(request);

            if (user == null)
                return BadRequest();
            
            var token = await _loginService.GenerateTokenAsync(user.Id);
            
            return Ok(token);
        }

        [HttpPost]
        [Route("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            return Ok(true);
        }

        [HttpPost]
        [Route("password/forgotten")]
        public async Task<IActionResult> ForgottenPasswordAsync()
        {
            return Ok(true);
        }

        [HttpPost]
        [Route("password/reset")]
        public async Task<IActionResult> ResetPasswordAsync()
        {
            return Ok(true);
        }
    }
}
