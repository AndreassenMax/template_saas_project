﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SecureUserManagementSystem.Data;

namespace SecureUserManagementSystem.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContextConfiguration(
            this IServiceCollection services,
            IConfiguration configuration
            )
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            
            services.AddDbContext<UserManagementDbContext>(options =>
            {
                options
                    .UseLazyLoadingProxies()
                    .UseSqlite(connectionString);
            });

            return services;
        }
    }
}
