using Autofac;
using Foundation.Entities.Contracts;
using Microsoft.EntityFrameworkCore;
using SecureUserManagementSystem.Data;
using SecureUserManagementSystem.Data.Base;
using SecureUserManagementSystem.Domain.Users.Services;
using SecureUserManagementSystem.Domain.Users.Services.Contracts;
using SecureUserManagementSystem.Security.Services;

namespace SecureUserManagementSystem.Extensions
{
    public static class AutofacContainerBuilderExtensions
    {
        public static ContainerBuilder ConfigureDependencies(this ContainerBuilder builder)
        {
            builder.RegisterType<UserManagementDbContext>()
                .As<IWorkUnit>()
                .As<DbContext>()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<UserRegistrationService>()
                .As<IUserRegistrationService>();

            builder
                .RegisterType<LoginService>()
                .As<ILoginService>();

            builder
                .RegisterType<SecurityService>()
                .As<ISecurityService>();
            
            /*builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .WithAttribute<DomainServiceAttribute>()
                .AsImplementedInterfaces()
                .InstancePerDependency();*/
            
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>));

            return builder;
        }
    }
}