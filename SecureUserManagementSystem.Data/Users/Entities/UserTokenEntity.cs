using System;
using DelegateDecompiler;
using SecureUserManagementSystem.Data.Base;

namespace SecureUserManagementSystem.Data.Users.Entities
{
    public class UserTokenEntity : BaseEntity
    {
        public string Token { get; set; }

        public DateTime ExpiresAt { get; set; }

        public virtual UserEntity User { get; set; }
    }
}