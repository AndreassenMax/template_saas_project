using System;
using SecureUserManagementSystem.Data.Base;

namespace SecureUserManagementSystem.Data.Users.Entities
{
    public class UserEntity : BaseEntity
    {
        private const int BCryptWorkFactor = 8;
        
        public string Username { get; set; }
        
        public string Email { get; set; }

        public string PasswordHash { get; set; }
        
        public void ChangePassword(string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                throw new ArgumentNullException(nameof(newPassword), "A password cannot be null or empty.");

            PasswordHash = BCrypt.Net.BCrypt.HashPassword(newPassword, BCrypt.Net.BCrypt.GenerateSalt(BCryptWorkFactor));
        }

        public bool PasswordIsCorrect(string password)
        {
            return !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(PasswordHash) && BCrypt.Net.BCrypt.Verify(password, PasswordHash);
        }
    }
}