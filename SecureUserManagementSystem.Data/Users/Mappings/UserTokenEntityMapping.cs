using Foundation.Entities.Mapping;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SecureUserManagementSystem.Data.Users.Entities;

namespace SecureUserManagementSystem.Data.Users.Mappings
{
    public class UserTokenEntityMapping : EntityMappingConfiguration<UserTokenEntity>
    {
        public override void Configure(EntityTypeBuilder<UserTokenEntity> builder)
        {
            builder
                .HasOne(e => e.User)
                .WithMany();
        }
    }
}