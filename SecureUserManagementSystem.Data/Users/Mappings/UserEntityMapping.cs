
using Foundation.Entities.Mapping;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SecureUserManagementSystem.Data.Users.Entities;

namespace SecureUserManagementSystem.Data.Users.Mappings
{
    public class UserEntityMapping : EntityMappingConfiguration<UserEntity>
    {
        public override void Configure(EntityTypeBuilder<UserEntity> builder)
        {
        }
    }
}