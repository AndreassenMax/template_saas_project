﻿using Microsoft.EntityFrameworkCore;
using SecureUserManagementSystem.Data.Base;
using SecureUserManagementSystem.Data.Users.Entities;

namespace SecureUserManagementSystem.Data
{
    public class UserManagementDbContext : BaseDataContext
    {
        public DbSet<UserEntity> Users { get; set; }
        
        public DbSet<UserTokenEntity> UserTokens { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        public override void Seed()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public UserManagementDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}