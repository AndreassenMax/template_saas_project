namespace SecureUserManagementSystem.Domain.Users
{
    public static class BCryptSettings
    {
        public static int BCryptWorkFactor = 8;
    }
}