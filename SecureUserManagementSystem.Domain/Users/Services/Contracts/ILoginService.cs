using System.Threading.Tasks;
using SecureUserManagementSystem.Data.Users.Entities;
using SecureUserManagementSystem.Domain.Users.Requests;

namespace SecureUserManagementSystem.Domain.Users.Services.Contracts
{
    public interface ILoginService
    {
        Task<string> GenerateTokenAsync(int userId);

        Task<UserEntity> GetUserAsync(LoginRequest request);
    }
}