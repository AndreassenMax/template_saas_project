using System.Threading.Tasks;
using SecureUserManagementSystem.Data.Users.Entities;

namespace SecureUserManagementSystem.Domain.Users.Services.Contracts
{
    public interface ISecurityService
    {
        Task<UserEntity> GetCurrentUserAsync();
    }
}