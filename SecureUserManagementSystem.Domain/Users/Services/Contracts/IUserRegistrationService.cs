using System.Threading.Tasks;
using SecureUserManagementSystem.Domain.Users.Requests;

namespace SecureUserManagementSystem.Domain.Users.Services.Contracts
{
    public interface IUserRegistrationService
    {
        Task<bool> RegisterAsync(RegistrationRequest request);
        
        Task<string> TestAsync();
    }
}