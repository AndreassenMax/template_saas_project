using System.Threading.Tasks;
using Foundation.Configuration;
using Foundation.Entities.Contracts;
using Microsoft.EntityFrameworkCore;
using SecureUserManagementSystem.Data.Users.Entities;
using SecureUserManagementSystem.Domain.Users.Requests;
using SecureUserManagementSystem.Domain.Users.Services.Contracts;

namespace SecureUserManagementSystem.Domain.Users.Services
{
    [DomainService]
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly IRepository<UserEntity> _userRepository;
        private readonly IWorkUnit _workUnit;
        private readonly ISecurityService _securityService;

        public UserRegistrationService(
            IRepository<UserEntity> userRepository,
            IWorkUnit workUnit,
            ISecurityService securityService
            )
        {
            _userRepository = userRepository;
            _workUnit = workUnit;
            _securityService = securityService;
        }

        public async Task<bool> RegisterAsync(RegistrationRequest request)
        {
            var existing = await _userRepository
                .FirstOrDefaultAsync(p => p.Email == request.Email || p.Username == request.Username);

            if (existing != null)
                return false;
            
            var entity = _userRepository.Create();
            entity.Email = request.Email;
            entity.Username = request.Username;
            
            if (!string.IsNullOrEmpty(request.Password))
                entity.ChangePassword(request.Password);

            await _workUnit.CommitAsync();

            return true;
        }
        
        public async Task<string> TestAsync()
        {
            return (await _securityService.GetCurrentUserAsync()).Username;
        }
    }
}